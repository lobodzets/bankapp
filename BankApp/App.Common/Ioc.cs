﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace App.Common
{
   
    public sealed class Ioc
    {
        private static readonly Lazy<IUnityContainer> lazy = new Lazy<IUnityContainer>(() => new UnityContainer());        

        public static IUnityContainer Instance
        {
            get
            {
                return lazy.Value;
            }
        }
              
    }
    
}
