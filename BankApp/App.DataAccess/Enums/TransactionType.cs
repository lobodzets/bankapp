﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DataAccess
{
    public enum TransactionType
    {
        DepositMoney,
        WithdrawMoney,
        TransferMoney
    }
}
