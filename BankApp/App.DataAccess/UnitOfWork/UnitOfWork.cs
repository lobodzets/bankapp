﻿using System;

namespace App.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private IBankDbContext _context;
        private Repository<Client> _clientRepository;
        private Repository<Transaction> _transactionRepository;       

        public UnitOfWork(IBankDbContext Context)
        {
            _context = Context;                        
        }

        public Repository<Client> ClientRepository
        {
            get
            {
                if (_clientRepository == null) _clientRepository = new Repository<Client>(_context, _context.Clients);
                return _clientRepository;
            }
        }

        public Repository<Transaction> TransactionRepository
        {
            get
            {
                if (_transactionRepository == null)
                    _transactionRepository = new Repository<Transaction>(_context, _context.Transactions);
                return _transactionRepository;
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();            
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

    }
}
