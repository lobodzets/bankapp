﻿using System;

namespace App.DataAccess
{
    public interface IUnitOfWork: IDisposable
    {
        Repository<Client> ClientRepository { get; }
        Repository<Transaction> TransactionRepository { get; }
        void SaveChanges();
    }
}

