﻿using System.Data.Entity;

namespace App.DataAccess
{
    public class BankDbContext : DbContext, IBankDbContext
    {
        public IDbSet<Client> Clients { get; set; }
        public IDbSet<Transaction> Transactions { get; set; }

        public BankDbContext() : base("name=BankDbContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Client>().Property(x => x.RowVersion).IsConcurrencyToken();

            /*
               [Timestamp]
               public byte[] RowVersion { get; set; }
            
               modelBuilder.Entity<Client>().Property(x => x.Balance).IsConcurrencyToken();
            */

            // context.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);

            // context.Entry<Customer>(obj).State = EntityState.Modified;

            /*
             using(var scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    // Do something 
                    context.SaveChanges();
                    // Do something else
                    context.SaveChanges();

                    scope.Complete();
                }
             */


        }


        void IBankDbContext.SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
