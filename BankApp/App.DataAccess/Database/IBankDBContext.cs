﻿using System;
using System.Data.Entity;

namespace App.DataAccess
{
    public interface IBankDbContext: IDisposable
    {
        IDbSet<Client> Clients { get; set; }
        IDbSet<Transaction> Transactions { get; set; }
        void SaveChanges();
    }
}
