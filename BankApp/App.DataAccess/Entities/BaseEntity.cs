﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }        
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }        

        public BaseEntity()
        {
            Created = DateTime.Now;
            Modified = DateTime.Now;
        }
    }
}
