﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess
{
    public class Client: BaseEntity
    {
        [Required(ErrorMessage = "Enter client name.")]
        public string Name { get; set; }        
        [Required(ErrorMessage = "Enter client password")]
        public string Password { get; set; }
        [Required]
        [DefaultValue(0)]
        [ConcurrencyCheck]
        public Decimal Balance { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
