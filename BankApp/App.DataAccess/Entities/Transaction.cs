﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess
{  
    public class Transaction: BaseEntity
    {        
        public Client ClientFrom { get; set; }
        public Client ClientTo { get; set; }
        [Required]        
        public Decimal Amount { get; set; }
        [Required]
        public TransactionType TransactionType { get; set; }
        [Required]
        public Client CreatedBy { get; set; }
        [Required]        
        public Client ModifiedBy { get; set; }

    }
}
