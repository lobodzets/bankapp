﻿using System.Data.Entity;
using System.Linq;

namespace App.DataAccess
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IBankDbContext _context;
        private IDbSet<T> _entities;
        public string ErrorMessage { get; private set; }

        public Repository(IBankDbContext Context, IDbSet<T> Entities)
        {
            _context = Context;
            _entities = Entities;
        }

        public IQueryable<T> GetAll()
        {
            return _entities                
                .AsQueryable<T>();
        }

        public T GetById(int id)
        {
            return _entities.Find(id);
        }

        public void Insert(T entity)
        {
            _entities.Add(entity);           
        }        

        public void Delete(T entity)
        {            
            _entities.Remove(entity);                           
        }


        public void SaveChanges()
        {
            _context.SaveChanges();
        }

    }
}
