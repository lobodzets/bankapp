﻿using System.Linq;

namespace App.DataAccess
{
    public interface IRepository<T> where T: BaseEntity
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Insert(T entity);        
        void Delete(T entity);
        void SaveChanges();
    }
}
