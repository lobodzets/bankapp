namespace App.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBaseEntityCreatedBy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "CreatedBy_Id", c => c.Int());
            AddColumn("dbo.Transactions", "ModifiedBy_Id", c => c.Int());
            CreateIndex("dbo.Transactions", "CreatedBy_Id");
            CreateIndex("dbo.Transactions", "ModifiedBy_Id");
            AddForeignKey("dbo.Transactions", "CreatedBy_Id", "dbo.Clients", "Id");
            AddForeignKey("dbo.Transactions", "ModifiedBy_Id", "dbo.Clients", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "ModifiedBy_Id", "dbo.Clients");
            DropForeignKey("dbo.Transactions", "CreatedBy_Id", "dbo.Clients");
            DropIndex("dbo.Transactions", new[] { "ModifiedBy_Id" });
            DropIndex("dbo.Transactions", new[] { "CreatedBy_Id" });
            DropColumn("dbo.Transactions", "ModifiedBy_Id");
            DropColumn("dbo.Transactions", "CreatedBy_Id");
        }
    }
}
