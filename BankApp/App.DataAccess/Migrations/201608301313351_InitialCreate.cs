namespace App.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionType = c.Int(nullable: false),
                        ClientFrom_Id = c.Int(),
                        ClientTo_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientFrom_Id)
                .ForeignKey("dbo.Clients", t => t.ClientTo_Id)
                .Index(t => t.ClientFrom_Id)
                .Index(t => t.ClientTo_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "ClientTo_Id", "dbo.Clients");
            DropForeignKey("dbo.Transactions", "ClientFrom_Id", "dbo.Clients");
            DropIndex("dbo.Transactions", new[] { "ClientTo_Id" });
            DropIndex("dbo.Transactions", new[] { "ClientFrom_Id" });
            DropTable("dbo.Transactions");
            DropTable("dbo.Clients");
        }
    }
}
