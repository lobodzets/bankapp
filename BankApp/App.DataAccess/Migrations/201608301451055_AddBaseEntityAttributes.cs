namespace App.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBaseEntityAttributes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Transactions", "CreatedBy_Id", "dbo.Clients");
            DropForeignKey("dbo.Transactions", "ModifiedBy_Id", "dbo.Clients");
            DropIndex("dbo.Transactions", new[] { "CreatedBy_Id" });
            DropIndex("dbo.Transactions", new[] { "ModifiedBy_Id" });
            AlterColumn("dbo.Clients", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Clients", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Transactions", "CreatedBy_Id", c => c.Int(nullable: true));
            AlterColumn("dbo.Transactions", "ModifiedBy_Id", c => c.Int(nullable: true));
            CreateIndex("dbo.Transactions", "CreatedBy_Id");
            CreateIndex("dbo.Transactions", "ModifiedBy_Id");
            AddForeignKey("dbo.Transactions", "CreatedBy_Id", "dbo.Clients", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Transactions", "ModifiedBy_Id", "dbo.Clients", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "ModifiedBy_Id", "dbo.Clients");
            DropForeignKey("dbo.Transactions", "CreatedBy_Id", "dbo.Clients");
            DropIndex("dbo.Transactions", new[] { "ModifiedBy_Id" });
            DropIndex("dbo.Transactions", new[] { "CreatedBy_Id" });
            AlterColumn("dbo.Transactions", "ModifiedBy_Id", c => c.Int());
            AlterColumn("dbo.Transactions", "CreatedBy_Id", c => c.Int());
            AlterColumn("dbo.Clients", "Password", c => c.String());
            AlterColumn("dbo.Clients", "Name", c => c.String());
            CreateIndex("dbo.Transactions", "ModifiedBy_Id");
            CreateIndex("dbo.Transactions", "CreatedBy_Id");
            AddForeignKey("dbo.Transactions", "ModifiedBy_Id", "dbo.Clients", "Id");
            AddForeignKey("dbo.Transactions", "CreatedBy_Id", "dbo.Clients", "Id");
        }
    }
}
