namespace App.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBaseEntityModified : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clients", "Modified", c => c.DateTime(nullable: false));
            AddColumn("dbo.Transactions", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Transactions", "Modified", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "Modified");
            DropColumn("dbo.Transactions", "Created");
            DropColumn("dbo.Clients", "Modified");
            DropColumn("dbo.Clients", "Created");
        }
    }
}
