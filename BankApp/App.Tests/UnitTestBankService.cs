﻿using System.Linq;
using App.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using App.Common;
using App.DataAccess;
using Microsoft.Practices.Unity;

namespace App.Tests
{
    [TestClass]
    public class UnitTestBankService
    {

        public UnitTestBankService()
        {
            Ioc.Instance.RegisterType<IUnitOfWork, UnitOfWork>();
            Ioc.Instance.RegisterType<IBankDbContext, BankDbContext>();
            Ioc.Instance.RegisterType<IClientService, ClientService>();
            Ioc.Instance.RegisterType<IBankService, BankService>();
        }
        
        [TestMethod]
        public void TestDepositMoney()
        {
            IClientService clientService = Ioc.Instance.Resolve<IClientService>();
            IBankService bankService = Ioc.Instance.Resolve<IBankService>();
            var clientId = 9;
            var client = clientService.GetClientById(clientId).Obj;
            var depositAmount = 200;
            var balanceBefore = client.Balance;

            bankService.DepositMoney(clientId, depositAmount);
            client = clientService.GetClientById(clientId).Obj;
            var balanceAfter = client.Balance;

            Assert.AreEqual(balanceAfter, balanceBefore + depositAmount);
        }

        [TestMethod]
        public void WithdrawMoney()
        {
            IClientService clientService = Ioc.Instance.Resolve<IClientService>();
            IBankService bankService = Ioc.Instance.Resolve<IBankService>();
            var clientId = 9;
            var client = clientService.GetClientById(clientId).Obj;
            var withdrawAmount = 100;
            var balanceBefore = client.Balance;

            bankService.WithdrawMoney(clientId, withdrawAmount);
            client = clientService.GetClientById(clientId).Obj;
            var balanceAfter = client.Balance;

            Assert.AreEqual(balanceAfter, balanceBefore - withdrawAmount);
        }

        
        [TestMethod]
        public void TransferMoney()
        {
            IClientService clientService = Ioc.Instance.Resolve<IClientService>();
            IBankService bankService = Ioc.Instance.Resolve<IBankService>();

            var clientFrom = clientService.GetClientById(9).Obj;
            var clientTo = clientService.GetClientById(10).Obj;

            var clientFromBalanceBefore = clientFrom.Balance;
            var clientToBalanceBefore = clientTo.Balance;
            var transferAmount = 50;

            bankService.TransferMoney(9, 10, transferAmount);

            clientFrom = clientService.GetClientById(9).Obj;
            clientTo = clientService.GetClientById(10).Obj;

            var clientFromBalanceAfter = clientFrom.Balance;
            var clientToBalanceAfter = clientTo.Balance;

            Assert.AreEqual(clientFromBalanceAfter, clientFromBalanceBefore - transferAmount);
            Assert.AreEqual(clientToBalanceAfter, clientToBalanceBefore + transferAmount);
        }

        [TestMethod]
        public void GetTransactionsByClientId()
        {
            IClientService clientService = Ioc.Instance.Resolve<IClientService>();
            IBankService bankService = Ioc.Instance.Resolve<IBankService>();

            var transactions = bankService.GetTransactionsByClientId(9);
            Assert.IsTrue(transactions.Objs.Any());
        }

        [TestMethod]
        public void GetAllTransactions()
        {
            IClientService clientService = Ioc.Instance.Resolve<IClientService>();
            IBankService bankService = Ioc.Instance.Resolve<IBankService>(); ;

            var transactions = bankService.GetAllTransactions();
            Assert.IsTrue(transactions.Objs.Any());
        }

        [TestMethod]
        public void GetAllClients()
        {
            IClientService clientService = Ioc.Instance.Resolve<IClientService>();
            IBankService bankService = Ioc.Instance.Resolve<IBankService>();

            var clients = clientService.GetAllClients();
            Assert.IsTrue(clients.Objs.Any());
        }


    }
}
