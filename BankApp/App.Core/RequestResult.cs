﻿using System.Collections.Generic;

namespace App.Core
{
    public enum RequestResultStatusEnum { Success, Error }

    public class RequestResult
    {
        public RequestResultStatusEnum Status { get; set; }
        public string Message { get; set; }
    }

    public class RequestResultStatus<T> : RequestResult
    {
        public T Obj { get; set; }
    }

    public class RequestResultStatusList<T> : RequestResult
    {
        public IEnumerable<T> Objs { get; set; }        
    }
}
