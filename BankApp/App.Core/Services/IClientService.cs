﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.DataAccess;

namespace App.Core
{
    public interface IClientService
    {
        RequestResultStatus<Client> GetClientById(int Id);
        RequestResultStatusList<Client> GetAllClients();     
    }
}
