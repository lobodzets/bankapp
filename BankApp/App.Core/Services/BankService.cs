﻿using System;
using System.Data.Entity;
using System.Linq;
using App.Common;
using App.DataAccess;
using Microsoft.Practices.Unity;

namespace App.Core
{
    public class BankService: IBankService
    {        
        
        public RequestResult DepositMoney(int ClientId, decimal Amount)
        {
            RequestResult result = new RequestResult();
            result = AddTransaction(TransactionType.DepositMoney, -1, ClientId, Amount);
            return result;
        }

        public RequestResult WithdrawMoney(int ClientId, decimal Amount)
        {
            RequestResult result = new RequestResult();
            result = AddTransaction(TransactionType.WithdrawMoney, ClientId, -1, Amount);
            return result;
        }

        public RequestResult TransferMoney(int ClientIdFrom, int ClientIdTo, decimal Amount)
        {
            RequestResult result = new RequestResult();
            result = AddTransaction(TransactionType.TransferMoney, ClientIdFrom, ClientIdTo, Amount);
            return result;
        }
        

        private RequestResult AddTransaction(TransactionType transactionType, int ClientIdFrom, int ClientIdTo, decimal Amount)
        {
            try
            {
                using (var unitOfWork = Ioc.Instance.Resolve<IUnitOfWork>())
                {
                    Client clientFrom = null;
                    Client clientTo = null;

                    if (transactionType == TransactionType.WithdrawMoney || transactionType == TransactionType.TransferMoney)
                    {
                        clientFrom = unitOfWork.ClientRepository.GetById(ClientIdFrom);
                        if (clientFrom == null)
                        {                            
                            return CreateErrorResult("Client with ID " + clientFrom.Id + " not found.");
                        }

                        if (clientFrom.Balance < Amount)
                        {                            
                            return CreateErrorResult("Client doesn't have enough money.");
                        }
                    }

                    if (transactionType == TransactionType.DepositMoney || transactionType == TransactionType.TransferMoney)
                    {
                        clientTo = unitOfWork.ClientRepository.GetById(ClientIdTo);
                        if (clientTo == null)
                        {                            
                            return CreateErrorResult("Client with ID " + clientFrom.Id + " not found.");
                        }
                    }


                    unitOfWork.TransactionRepository.Insert(new Transaction
                    {
                        ClientFrom = clientFrom,
                        ClientTo = clientTo,
                        Amount = Amount,
                        TransactionType = transactionType,
                        CreatedBy = clientFrom ?? clientTo,
                        ModifiedBy = clientFrom ?? clientTo
                    });

                    switch (transactionType)
                    {
                        case TransactionType.DepositMoney: { clientTo.Balance += Amount; break; }
                        case TransactionType.WithdrawMoney: { clientFrom.Balance -= Amount; break; }
                        case TransactionType.TransferMoney: { clientFrom.Balance -= Amount; clientTo.Balance += Amount; break; }
                    }

                    unitOfWork.SaveChanges();

                    return CreateSuccessResult();

                }
            } catch (Exception ex)
            {                
                return CreateErrorResult(ex.Message);
            }            
        }

        private RequestResult CreateErrorResult (string msg)
        {
            return new RequestResult { Status = RequestResultStatusEnum.Error, Message = msg };
        }

        private RequestResult CreateSuccessResult(string msg = "")
        {
            return new RequestResult { Status = RequestResultStatusEnum.Success, Message = msg };
        }

        public RequestResultStatusList<Transaction> GetAllTransactions()
        {
            var result = new RequestResultStatusList<Transaction>();

            using (var unitOfWork = Ioc.Instance.Resolve<IUnitOfWork>())
            {
                result.Objs = unitOfWork.TransactionRepository.GetAll()
                    .Include(x => x.ClientFrom)
                    .Include(x => x.ClientTo)
                    .Include(x => x.CreatedBy)
                    .Include(x => x.ModifiedBy)
                    .ToList();

                result.Status = RequestResultStatusEnum.Success;
                return result;
            }
        }

        public RequestResultStatusList<Transaction> GetTransactionsByClientId(int ClientId)
        {
            var result = new RequestResultStatusList<Transaction>();

            using (var unitOfWork = Ioc.Instance.Resolve<IUnitOfWork>())
            {
                result.Objs = unitOfWork.TransactionRepository.GetAll()
                    .Include(x => x.ClientFrom)
                    .Include(x => x.ClientTo)
                    .Include(x => x.CreatedBy)
                    .Include(x => x.ModifiedBy)
                    .Where(x => x.ClientFrom.Id == ClientId || x.ClientTo.Id == ClientId)
                    .ToList();

                result.Status = RequestResultStatusEnum.Success;
                return result;
            }


        }



    }
}
