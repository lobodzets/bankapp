﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.DataAccess;

namespace App.Core
{
    public interface IBankService
    {
        RequestResult DepositMoney(int ClientId, decimal Amount);
        RequestResult WithdrawMoney(int ClientId, decimal Amount);
        RequestResult TransferMoney(int ClientIdFrom, int ClientIdTo, decimal Amount);        
        RequestResultStatusList<Transaction> GetAllTransactions();
        RequestResultStatusList<Transaction> GetTransactionsByClientId(int ClientId);
    }
}
