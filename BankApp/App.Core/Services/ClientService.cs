﻿using System.Data.Entity;
using System.Linq;
using App.Common;
using App.DataAccess;
using Microsoft.Practices.Unity;

namespace App.Core
{
    public class ClientService: IClientService
    {

        public RequestResultStatus<Client> GetClientById(int Id)
        {
            var result = new RequestResultStatus<Client>();

            using (var unitOfWork = Ioc.Instance.Resolve<IUnitOfWork>())
            {
                result.Obj = unitOfWork.ClientRepository.GetById(Id);

                result.Status = RequestResultStatusEnum.Success;
                return result;
            }
        }

        public RequestResultStatusList<Client> GetAllClients()
        {
            var result = new RequestResultStatusList<Client>();

            using (var unitOfWork = Ioc.Instance.Resolve<IUnitOfWork>())
            {
                result.Objs = unitOfWork.ClientRepository.GetAll().ToList();

                result.Status = RequestResultStatusEnum.Success;
                return result;
            }
        }
        
    }
}
