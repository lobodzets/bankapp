﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.Web.Models
{
    public class WithdrawMoney
    {
        public int ClientId { get; set; }
        [Required]
        public decimal Amount { get; set; }

        public WithdrawMoney()
        {

        }

        public WithdrawMoney(int clientId)
        {
            ClientId = ClientId;            
        }
    }
}