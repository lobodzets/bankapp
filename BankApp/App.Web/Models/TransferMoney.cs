﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.Web.Models
{
    public class TransferMoney
    {
        public int ClientFromId { get; set; }
        public int ClientToId { get; set; }
        [Required]
        public decimal Amount { get; set; }

        public TransferMoney()
        {

        }

        public TransferMoney(int clientFromId)
        {
            ClientFromId = clientFromId;                        
        }

        public TransferMoney(int clientFromId, int clientToId)
        {
            ClientFromId = clientFromId;
            ClientToId = clientToId;            
        }
    }
}