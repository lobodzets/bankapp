﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Core;

namespace App.Web.Controllers
{
    public class ClientsController : Controller
    {
        private ClientService _clientService;
        private BankService _bankService;

        public ClientsController(ClientService ClientService, BankService BankService)
        {
            _clientService = ClientService;
            _bankService = BankService;
        }

        // GET: Clients
        public ActionResult Index()
        {
            var clients = _clientService.GetAllClients().Objs;
            return View(clients);
        }        
        
        
    }
}
