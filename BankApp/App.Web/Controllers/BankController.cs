﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Core;
using App.Web.Models;

namespace App.Web.Controllers
{
    public class BankController : Controller
    {
        private BankService _bankService;
        private ClientService _clientService;

        public BankController(BankService BankService, ClientService ClientService)
        {
            _bankService = BankService;
            _clientService = ClientService;
        }

        // GET: Bank/Transactions
        public ActionResult Transactions(int clientId)
        {
            var transactions = _bankService.GetTransactionsByClientId(clientId).Objs.OrderByDescending(x => x.Created);
            return View(transactions);
        }

        // GET: Bank/DepositMoney
        public ActionResult DepositMoney(int clientId)
        {
            return View(new DepositModel(clientId));
        }

        // POST: Bank/DepositMoney
        [HttpPost]
        public ActionResult DepositMoney(DepositModel model)
        {
            _bankService.DepositMoney(model.ClientId, model.Amount);
            return RedirectToAction("Transactions", "Bank", new { clientId = model.ClientId });
        }

        // GET: Bank/WithdrawMoney
        public ActionResult WithdrawMoney(int clientId)
        {
            return View(new WithdrawMoney(clientId));
        }

        // POST: Bank/WithdrawMoney
        [HttpPost]
        public ActionResult WithdrawMoney(WithdrawMoney model)
        {
            _bankService.WithdrawMoney(model.ClientId, model.Amount);
            return RedirectToAction("Transactions", "Bank", new { clientId = model.ClientId });
        }


        // GET: Bank/TransferMoney
        public ActionResult TransferMoney(int clientId)
        {            
            return View(new TransferMoney(clientId));
        }

        // POST: Bank/TransferMoney
        [HttpPost]
        public ActionResult TransferMoney(TransferMoney model)
        {
            _bankService.TransferMoney(model.ClientFromId, model.ClientToId, model.Amount);
            return RedirectToAction("Transactions", "Bank", new { clientId = model.ClientFromId });
        }

    }
}
